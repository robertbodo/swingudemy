package Controller;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import GUI.FormEvent;
import Model.AgeCategory;
import Model.Database;
import Model.EmploymentCategory;
import Model.Gender;
import Model.Person;

public class Controller {
	Database db = new Database();

	public List<Person> getPeople() {
		return db.getPeople();
	}

	public void removePerson(int index) {
		db.removePerson(index);
	}

	public void save() throws SQLException {
		db.save();
	}

	public void connect() throws Exception {
		db.connect();
	}

	public void disconnect() {
		db.disconnect();
	}

	public void load() throws SQLException {
		db.load();
	}

	public void addPerson(FormEvent ev) {
		String name = ev.getName();
		String occupation = ev.getOccupation();
		int ageCatId = ev.getAgeCategory();
		String empCat = ev.getEmpCat();
		boolean isUs = ev.isUsCitizen();
		String taxId = ev.getTaxId();
		String gender = ev.getGender();

		AgeCategory ageCategory = null;

		switch (ageCatId) {
		case 0:
			ageCategory = AgeCategory.UNDER_18;
			break;
		case 1:
			ageCategory = AgeCategory.ADULT;
			break;
		case 2:
			ageCategory = AgeCategory.SENIOR;
			break;
		}

		EmploymentCategory employmentCategory;
		if (empCat.equals("employed")) {
			employmentCategory = EmploymentCategory.EMPLOYED;
		} else if (empCat.equals("self-employed")) {
			employmentCategory = EmploymentCategory.SELFEMPLOYED;
		} else if (empCat.equals("unemployed")) {
			employmentCategory = EmploymentCategory.UNEMPLOYED;
		} else {
			employmentCategory = EmploymentCategory.OTHER;
			System.err.println(empCat);
		}

		Gender genderCat;
		if (gender.equalsIgnoreCase("Male")) {
			genderCat = Gender.MALE;
		} else {
			genderCat = Gender.FEMALE;
		}

		Person person = new Person(name, occupation, ageCategory, employmentCategory, taxId, isUs, genderCat);
		db.addPerson(person);
	}

	public void saveToFile(File file) throws IOException {
		db.saveToFile(file);
	}

	public void loadFromFile(File file) throws IOException {
		db.loadFromFile(file);
	}

}
