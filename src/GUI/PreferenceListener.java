package GUI;

public interface PreferenceListener {
	public void preferencesSet(String user, String password, int portNumber);
}
