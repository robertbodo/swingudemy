package GUI;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import Model.Person;

public class TablePanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7665723726342110686L;
	private JTable table;
	private PersonTableModel tableModel;
	private JPopupMenu popUp;
	private PersonTableListener personTableListener;

	public TablePanel() {
		tableModel = new PersonTableModel();
		table = new JTable(tableModel);
		popUp = new JPopupMenu();

		JMenuItem removeItem = new JMenuItem("Delete row");
		popUp.add(removeItem);
		table.addMouseListener(new MouseAdapter() {

			@Override
			public void mousePressed(MouseEvent event) {
				int row = table.rowAtPoint(event.getPoint());
				table.getSelectionModel().setSelectionInterval(row, row);
				System.out.println(row);
				if (event.getButton() == MouseEvent.BUTTON3) {
					popUp.show(table, event.getX(), event.getY());
				}
			}

		});

		removeItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int row = table.getSelectedRow();
				if (personTableListener != null) {
					personTableListener.rowDeleted(row);
					tableModel.fireTableRowsDeleted(row, row);
				}
			}
		});

		setLayout(new BorderLayout());
		add(new JScrollPane(table), BorderLayout.CENTER);
	}

	public void setData(List<Person> db) {
		tableModel.setData(db);
	}

	public void refresh() {
		tableModel.fireTableDataChanged();
	}

	public void setPersonTableListener(PersonTableListener listener) {
		this.personTableListener = listener;
	}
}
