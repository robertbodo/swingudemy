package GUI;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JToolBar;

public class Toolbar extends JToolBar implements ActionListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton saveButton;
	private JButton refreshButton;

	private ToolBarListener textListener;

	public void setToolbarListener(ToolBarListener listener) {
		this.textListener = listener;
	}

	public Toolbar() {
		// Comment this line if you wish to toolbar to be draggable
		setBorder(BorderFactory.createEtchedBorder());
		// setFloatable(false);
		saveButton = new JButton("");
		saveButton.setIcon(createIcon("/images/Save16.gif"));
		saveButton.setToolTipText("Save");

		refreshButton = new JButton("");
		refreshButton.setIcon(createIcon("/images/Refresh16.gif"));
		refreshButton.setToolTipText("Refressh");

		saveButton.addActionListener(this);
		refreshButton.addActionListener(this);

		setLayout(new FlowLayout(FlowLayout.LEFT));

		add(saveButton);
		add(refreshButton);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton clicked = (JButton) e.getSource();
		if (clicked == saveButton) {
			if (this.textListener != null) {
				textListener.saveEventOccured();
			}
		} else if (clicked == refreshButton) {
			if (this.textListener != null) {
				textListener.refreshEventOccured();
			}
		}

	}

	private ImageIcon createIcon(String path) {
		URL url = getClass().getResource(path);
		if (url == null) {
			System.err.println("Unable to load url image " + path);
		}
		ImageIcon icon = new ImageIcon(url);
		return icon;

	}

}
