package Model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;

public class Database {
	private LinkedList<Person> people;

	private Connection con;

	public Database() {
		people = new LinkedList<Person>();
	}

	public void connect() throws Exception {
		if (con != null)
			return;
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			throw new Exception("Drive not found");
		}
		String connectionUrl = "jdbc:mysql://localhost:3306/swingtest?autoReconnect=true&useSSL=false";
		con = DriverManager.getConnection(connectionUrl, "root", "ronaldo15");
		System.out.println("Connected to " + connectionUrl);

	}

	public void disconnect() {
		if (con != null) {
			try {
				con.close();
				System.out.println("Connection closed without error");
			} catch (SQLException e) {
				System.out.println("Close Conection Failed");
			}
		}
	}

	public void save() throws SQLException {
		String checkSQL = "select count(*)from people where id=?";
		PreparedStatement checkStatement = (PreparedStatement) con.prepareStatement(checkSQL);

		String insertSQL = "INSERT INTO people(id, name, age, employment_status, tax_id, us_citizen, gender, occupation) "
				+ "VALUES (?,?,?,?,?,?,?,?);";
		PreparedStatement insertStatement = (PreparedStatement) con.prepareStatement(insertSQL);

		String updateSQL = "UPDATE people SET name=?, age=?, employment_status=?, tax_id=?, us_citizen=?, gender=?, occupation=? "
				+ "WHERE id=?";
		PreparedStatement updateStatement = (PreparedStatement) con.prepareStatement(updateSQL);

		for (Person person : people) {

			int id = person.getId();
			String name = person.getName();
			String occupation = person.getOccupation();
			AgeCategory age = person.getAgeCategory();
			EmploymentCategory emp = person.getEmpCat();
			String tax = person.getTaxId();
			boolean isUs = person.isUsCitizen();
			Gender gender = person.getGender();

			checkStatement.setInt(1, id);

			ResultSet checkResult = checkStatement.executeQuery();
			checkResult.next();
			int count = checkResult.getInt(1);

			if (count == 0) {
				System.out.println("inserting person with id");
				int col = 1;
				insertStatement.setInt(col++, id);
				insertStatement.setString(col++, name);
				insertStatement.setString(col++, age.name());
				insertStatement.setString(col++, emp.name());
				insertStatement.setString(col++, tax);
				insertStatement.setBoolean(col++, isUs);
				insertStatement.setString(col++, gender.name());
				insertStatement.setString(col++, occupation);

				insertStatement.executeUpdate();
			} else {
				int col = 1;
				updateStatement.setString(col++, name);
				updateStatement.setString(col++, age.name());
				updateStatement.setString(col++, emp.name());
				updateStatement.setString(col++, tax);
				updateStatement.setBoolean(col++, isUs);
				updateStatement.setString(col++, gender.name());
				updateStatement.setString(col++, occupation);
				updateStatement.setInt(col++, id);

				updateStatement.executeUpdate();
			}
			System.out.println("Count for person with ID " + id + " is " + count);
		}
		updateStatement.close();
		insertStatement.close();
		checkStatement.close();
	}

	public void load() throws SQLException {
		people.clear();
		String sql = "SELECT id, name, age, employment_status, tax_id, us_citizen, gender, occupation FROM people ORDER BY name";
		Statement selectStatement = (Statement) con.createStatement();
		selectStatement.executeQuery(sql);
		ResultSet results = selectStatement.executeQuery(sql);
		while (results.next()) {
			int id = results.getInt("id");
			String name = results.getString("name");
			String age = results.getString("age");
			String emp = results.getString("employment_status");
			String tax = results.getString("tax_id");
			boolean isUS = results.getBoolean("us_citizen");
			String gender = results.getString("gender");
			String occupation = results.getString("occupation");
			Person person = new Person(id, name, occupation, AgeCategory.valueOf(age), EmploymentCategory.valueOf(emp),
					tax, isUS, Gender.valueOf(gender));
			people.add(person);
			System.out.println(person);
		}
		selectStatement.close();
	}

	public void addPerson(Person person) {
		people.add(person);
	}

	public void removePerson(int index) {
		people.remove(index);
	}

	public List<Person> getPeople() {
		return Collections.unmodifiableList(people);
	}

	public void saveToFile(File file) throws IOException {
		FileOutputStream fileOutputStream = new FileOutputStream(file);
		ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);

		Person[] persons = people.toArray(new Person[people.size()]);
		objectOutputStream.writeObject(persons);
		objectOutputStream.close();
	}

	public void loadFromFile(File file) throws IOException {
		FileInputStream fileInputStream = new FileInputStream(file);
		ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
		try {
			Person[] persons = (Person[]) objectInputStream.readObject();
			people.clear();
			people.addAll(Arrays.asList(persons));
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		objectInputStream.close();
	}

}
