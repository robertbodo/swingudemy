package Model;

public enum EmploymentCategory {
	EMPLOYED, SELFEMPLOYED, UNEMPLOYED, OTHER;
}
