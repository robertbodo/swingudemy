import java.sql.SQLException;

import Model.AgeCategory;
import Model.Database;
import Model.EmploymentCategory;
import Model.Gender;
import Model.Person;

public class TestDatabase {

	public static void main(String[] args) {
		System.out.println("Running Database");
		Database db = new Database();
		try {
			db.connect();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		db.addPerson(
				new Person("Joe", "programmer", AgeCategory.ADULT, EmploymentCategory.EMPLOYED, "777", true, Gender.MALE));
		db.addPerson(new Person("Sue", "artist", AgeCategory.SENIOR, EmploymentCategory.SELFEMPLOYED, "999", true,
				Gender.FEMALE));
		try {
			db.save();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			db.load();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		db.disconnect();
	}

}
